﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrintVictoryMessage : MonoBehaviour {

    private bool isFinished;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        isFinished = GameObject.Find("Grid").GetComponent<VictoryHandler>().isFinished;

        GetComponent<Text>().enabled = isFinished;
	}
}
