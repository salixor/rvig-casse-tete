﻿using Leap;
using System.Collections;
using UnityEngine;

/// <summary>
/// Script de grab d'une brique entière, constituée de plusieurs sous-cubes
/// Il est nécessaire d'affecter une main à ce script
/// </summary>
public class BrickGrab : MonoBehaviour {
    //leap motion controller
    [Tooltip("Must be in the scene")]

    public HandController handController;
    public Material defaultMaterial;
    public Material hoveredMaterial;
    public KeyCode visibleHandKey = KeyCode.V;
    public bool defaultVisibleHand = false;

    private Hand hand;
    private HandModel handModel;
    private Renderer[] childRenderers;

    private bool grabbed = false;
    private bool visibleHand;

    private float previousStrength = 0.0f;
    private float currentStrength = 0.0f;
    private float grabStrength = 0.8f;
    private bool growingStrength = false;

    private bool canBeGrabbed = true;

    bool isVictory = false;

    public Vector3 Position {
        get;
        protected set;
    }

    public Quaternion Rotation {
        get;
        protected set;
    }

    public bool Grabbed {
        get { return grabbed; }
    }

    protected void Start() {
        Rotation = Quaternion.identity;
        Position = Vector3.zero;
        visibleHand = defaultVisibleHand;

        childRenderers = GetComponentsInChildren<Renderer>();
    }

    protected void Update() {
        UpdateTracker();

        // Mise à jour de l'état actuel de la partie
        isVictory = GameObject.FindGameObjectWithTag("Grid").GetComponent<VictoryHandler>().IsVictory;
        if (isVictory) return;

        canBeGrabbed = true;

        // Si une autre brique est déjà grab, on empêche le grab de celle ci
        foreach (GameObject grabbedBrick in GameObject.FindGameObjectsWithTag("BrickGrabbed"))
            if (grabbedBrick != this.gameObject)
                canBeGrabbed = false;

        // Si l'on ne peut pas hover ou grab, on force grabbed à false
        if (!canBeGrabbed)
            grabbed = false;

        // Récupération de l'ancienne force de grab et de l'actuelle
        previousStrength = currentStrength;
        currentStrength = (hand != null ? hand.GrabStrength : 0.0f);
        growingStrength = (currentStrength - previousStrength > 0);

        // Si pas de main ou plus assez de force de grab, pas de grab
        if (handModel == null || hand == null || hand.GrabStrength <= grabStrength)
            grabbed = false;

        // Material de base si l'objet est grab, ou qu'on ne peut pas le grab
        if (grabbed || !canBeGrabbed)
            foreach (Renderer rend in childRenderers)
                rend.material = defaultMaterial;

        // On met à jour le tag de grab
        tag = (canBeGrabbed && grabbed) ? "BrickGrabbed" : "Brick";

        // Enfin, on met à jour la position (si l'objet peut être grab et est grab)
        if (canBeGrabbed && grabbed) {
            transform.position = Vector3.Lerp(transform.position, Position, 0.2f);
            transform.rotation = Quaternion.Lerp(transform.rotation, Rotation, 0.2f);
        }
    }

    protected void UpdateTracker() {
        if (!handController) {
            Debug.LogError("Please set a HandController.");
            return;
        }

        //get the 1st hand in the frame
        if (handController.GetAllGraphicsHands().Length != 0) {
            handModel = handController.GetAllGraphicsHands()[0];

            MeshRenderer[] renderers = handModel.transform.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer rend in renderers)
                rend.enabled = visibleHand;

            hand = handModel.GetLeapHand();
            Position = handModel.GetPalmPosition();
            Rotation = handModel.GetPalmRotation();
        }

        //mask/display the graphical hand on key down
        if (Input.GetKeyDown(visibleHandKey))
            visibleHand = !visibleHand;
    }

    // Test de la main
    bool IsHand(Collision other) {
        return (other.transform.parent && other.transform.parent.parent && other.transform.parent.parent.GetComponent<HandModel>());
    }

    void OnCollisionStay(Collision other) {
        // On ne fait rien si la partie est gagnée
        if (isVictory) return;

        // Si on ne peut pas grab la brique, il ne sert pas de continuer
        if (!canBeGrabbed)
            return;

        // Si l'objet en collision est la main, on affiche que l'objet peut-être grab
        if (IsHand(other))
            foreach (Renderer rend in childRenderers)
                rend.material = hoveredMaterial;

        // Si l'objet n'est pas encore grab, on vérifie que la force de grab est suffisante et que le grab est croissant
        if (IsHand(other) && !grabbed)
            grabbed = (handModel != null && hand != null && hand.GrabStrength > grabStrength && previousStrength < grabStrength && growingStrength);

        // Si l'objet est déjà grab, on vérifie que la force de grab est encore suffisante
        if (IsHand(other) && grabbed)
            grabbed = (handModel != null && hand != null && hand.GrabStrength > grabStrength);
    }

    private void OnCollisionExit(Collision other) {
        // On ne fait rien si la partie est gagnée
        if (isVictory) return;

        foreach (Renderer rend in childRenderers)
            rend.material = defaultMaterial;
    }
}
