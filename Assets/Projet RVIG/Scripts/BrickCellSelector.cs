﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script de sélection d'une ou plusieurs cellules de la grille
/// </summary>
public class BrickCellSelector : MonoBehaviour {
    public Material defaultMaterial;
    public Material selectedMaterial;
    public Material errorMaterial;
    public GameObject cellPrefab;

    private bool currentlyGrabbed;
    private Vector3 absurdVector = new Vector3(-9999, -9999, -9999);

    private Vector3 selectedCellsCenter = new Vector3(); // Barycentre des cellules sélectionnées
    private List<GameObject> selectedCells = new List<GameObject>(); // Cellules sélectionnées

    bool isVictory = false;

    // Use this for initialization
    void Start () {
        selectedCellsCenter = absurdVector;
    }

    // Update is called once per frame
    void Update() {
        // Mise à jour de l'état actuel de la partie
        isVictory = GameObject.FindGameObjectWithTag("Grid").GetComponent<VictoryHandler>().IsVictory;
        if (isVictory) return;

        RaycastHit hit;

        int cellsLayerMask = 1 << 8; // Layer des cellules de la grille
        int cubesLayerMask = 1 << 9; // Layer des cubes pour les briques, quelle que soit la brique

        Vector3 roadInBrick = new Vector3(); // Chemin vectoriel à suivre dans la brique pour aller du premier cube au dernier

        // Rotation finale souhaitée de la brique, à 90° près sur chaque axe
        Vector3 rotationOfBrick = new Vector3(
            Mathf.Round(transform.rotation.eulerAngles.x / 90) * 90,
            Mathf.Round(transform.rotation.eulerAngles.y / 90) * 90,
            Mathf.Round(transform.rotation.eulerAngles.z / 90) * 90
        );

        // Récupération de l'état de grab de la brique
        currentlyGrabbed = GetComponent<BrickGrab>().Grabbed;

        // Remise à zéro des cellules de la grille sélectionnées auparavant
        foreach (GameObject previouslyHoveredCell in selectedCells)
            if (previouslyHoveredCell)
                previouslyHoveredCell.GetComponent<Renderer>().material = defaultMaterial;
        selectedCells.Clear();

        // Informations sur l'itération précédente
        GameObject lastHitCell = null;
        Vector3 lastLocalPosition = new Vector3();

        // Itération sur les cubes de la brique
        foreach (Transform t in transform) {
            GameObject cube = t.gameObject; // Cube actuel
            Vector3 localPosition = t.localPosition; // Position locale du cube
            Vector3 nextCandidatePosition = cube.transform.position; // Position fictive du cube à tester

            if (lastHitCell != null) {
                // Calcul de la direction du cube précédent au cube actuel, avec rotation
                Vector3 directionFromPreviousCube = localPosition - lastLocalPosition;
                directionFromPreviousCube = Quaternion.AngleAxis(rotationOfBrick.y, Vector3.up) * directionFromPreviousCube;

                // Ajout au vecteur du chemin à suivre pour aller du premier cube au dernier
                roadInBrick += directionFromPreviousCube;

                // Projection dans la grille de la position de la cellule candidate
                nextCandidatePosition = new Vector3(
                    lastHitCell.transform.position.x + directionFromPreviousCube.x,
                    cube.transform.position.y,
                    lastHitCell.transform.position.z + directionFromPreviousCube.z
                );
            }

            // Vérification de la possibilité de placer ce cube dans la grille
            if (Physics.Raycast(nextCandidatePosition, Vector3.down, out hit, Mathf.Infinity, cellsLayerMask)) {
                if (!hit.collider.gameObject.name.StartsWith(cellPrefab.name))
                    return;

                selectedCells.Add(hit.collider.gameObject);
                lastHitCell = hit.collider.gameObject;
            }

            // Vérification que ce cube ne serait pas placé à l'endroit d'une autre brique
            if (Physics.Raycast(nextCandidatePosition, Vector3.down, out hit, Mathf.Infinity, cubesLayerMask)) {
                if (selectedCells.Count > 0)
                    selectedCells.RemoveAt(selectedCells.Count - 1);

                lastHitCell = null;
            }

            lastLocalPosition = localPosition;
        }

        // Mise à jour du matériau pour les cellules sélectionnées
        foreach (GameObject previouslyHoveredCell in selectedCells) {
            if (selectedCells.Count != transform.childCount) // Pas assez de cellules libres
                previouslyHoveredCell.GetComponent<Renderer>().material = errorMaterial;
            else // Suffisamment de cellules libres
                previouslyHoveredCell.GetComponent<Renderer>().material = selectedMaterial;
        }

        // Si la brique est relachée, il est possible qu'elle doive se déplacer vers la sélection
        if (!currentlyGrabbed) {
            // Calcul du barycentre des cellules sélectionnées - position future de la brique
            if (selectedCells.Count == transform.childCount)
                selectedCellsCenter = selectedCells[0].transform.position + roadInBrick / 2;

            // Si il y a un barycentre des cellules sélectionnées, la brique s'y déplace
            if (selectedCellsCenter != absurdVector) {
                // Déplacement de la brique à l'endroit des cellules sélectionnées
                transform.position = Vector3.Lerp(transform.position, selectedCellsCenter, 0.2f);
                
                // Rotation de la brique de façon clippée
                Vector3 currentRotation = transform.rotation.eulerAngles;
                currentRotation.x = Mathf.Lerp(currentRotation.x, rotationOfBrick.x, 0.2f);
                currentRotation.y = Mathf.Lerp(currentRotation.y, rotationOfBrick.y, 0.2f);
                currentRotation.z = Mathf.Lerp(currentRotation.z, rotationOfBrick.z, 0.2f);
                transform.eulerAngles = currentRotation;

                // Si la brique est comme il faut, alors plus besoin de continuer
                if (transform.position == selectedCellsCenter && currentRotation == rotationOfBrick)
                    selectedCellsCenter = absurdVector;
            }
        }
    }
}
