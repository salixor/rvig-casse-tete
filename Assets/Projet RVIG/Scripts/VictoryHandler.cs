﻿using UnityEngine;

/// <summary>
/// Script de vérification de victoire en vérifiant que chaque cellule est remplie
/// </summary>
public class VictoryHandler : MonoBehaviour {
    private GameObject[] gridCells;
    private GameObject[] brickBlocks;

    public float authorizedDeviance = 0.01f;

    public bool isFinished = false;
    private bool gridCellValidated = false;

    public bool IsVictory {
        get { return isFinished; }
    }

    // Use this for initialization
    private void Start() {
        gridCells = GameObject.FindGameObjectsWithTag("GridCell");
        brickBlocks = GameObject.FindGameObjectsWithTag("BrickCube");
    }

    // Update is called once per frame
    private void Update() {
        if (isFinished)
            return;

        gridCellValidated = false;
        isFinished = false;

        foreach (GameObject gridCell in gridCells) {
            foreach (GameObject block in brickBlocks)
                gridCellValidated = gridCellValidated || ((gridCell.transform.position - block.transform.position).magnitude <= authorizedDeviance);

            if (!gridCellValidated)
                return;

            gridCellValidated = false;
        }

        isFinished = true;
    }
}